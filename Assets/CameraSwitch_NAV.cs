﻿using UnityEngine;
using System.Collections;

public class CameraSwitch_NAV : MonoBehaviour {
	
	
	public Camera FlightCamera;
	public Camera RadarCamera;
	public Camera MapCamera;
	public Camera RadioCamera;
	public Camera BombSight;
	
	
	private Camera[] NavCameras;
	private int currentNavCameraIndex = 0;
	private Camera currentNavCamera;
	

	void Start()
	
				{
						NavCameras = new Camera[] { FlightCamera, RadarCamera, MapCamera, RadioCamera };//this is the array of cameras
						currentNavCamera = FlightCamera; //When the program start the main camera is selected as the default camera
						ChangeView ();
				}
	
	// Update is called once per frame
	void Update()
	{
		if(Input.GetKeyDown("r"))
		{
			currentNavCameraIndex++;
			if (currentNavCameraIndex > NavCameras.Length-1)
				currentNavCameraIndex = 0;
			ChangeView();
		}
	}
	
	void ChangeView()
	{
		currentNavCamera.enabled = false;
		currentNavCamera = NavCameras[currentNavCameraIndex];
		currentNavCamera.enabled = true;
	}
	
}