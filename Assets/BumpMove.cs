﻿using UnityEngine;
using System.Collections;

public class BumpMove : MonoBehaviour {

	// Scroll main texture based on time
	
	public float scrollSpeed;
	
	void Update() { 
		float offset = Time.time * scrollSpeed;
		renderer.material.SetTextureOffset ("_BumpMap", new Vector2(offset,0));
	}
}
