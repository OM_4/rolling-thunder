﻿using UnityEngine;
using System.Collections;

public class PlaneTooCloseBruiser : MonoBehaviour {
	public GameObject plane;
	[HideInInspector] public AI_Bruiser harasserScript;
	
	void Awake(){
		harasserScript = plane.GetComponent<AI_Bruiser>();
	}
	
	void OnTriggerStay(Collider collideData) {
		if(collideData.CompareTag("Allied") || collideData.CompareTag ("AlliedPlayer")){
			harasserScript.setTooClose();
		}
		
	}	
}
