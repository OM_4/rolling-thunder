﻿using UnityEngine;
using System.Collections;

public class DeathScriptNew : MonoBehaviour {
	public Camera theCam;
	public GameObject rollingThunder;

	// Use this for initialization
	void Start () {
		theCam.enabled = false;
	}
	
	// Update is called once per frame
	void Update () {
		if (rollingThunder.activeInHierarchy == false) {
			theCam.enabled = true;
		}
	}
}
