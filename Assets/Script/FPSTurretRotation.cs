﻿using UnityEngine;
using System.Collections;

public class FPSTurretRotation : MonoBehaviour {
	public Camera myCamera;
	public Vector3 pos;


	void Start(){
		this.enabled = false;
	}

	// Update is called once per frame
	void LateUpdate () {
		pos = myCamera.transform.position;
		pos.y -= 0.09f;
		transform.position = pos;
		transform.rotation = myCamera.transform.rotation;
		//Debug.Log ("Script on");
	}
}
