using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Radar : MonoBehaviour {

	public GameObject[] trackedObjects;
	List<GameObject> radarObjects;
	public GameObject enemyPrefab;
	//List<GameObject> borderObjects;
	//public float switchDistance;
	public Transform helpTransform;

	// Use this for initialization
	void Start () {
		createRadarObjects ();
	}//end Start
	
	// Update is called once per frame
	void Update () {
//		for (int i = 0; i < radarObjects.Count; i++) {
//			if(Vector3.Distance(radarObjects[i].transform.position, transform.position) > switchDistance){
//				helpTransform.LookAt (radarObjects[i].transform);
//				borderObjects[i].transform.position = transform.position + switchDistance * helpTransform.forward;
//				borderObjects[i].layer = LayerMask.NameToLayer("RadarLayer");
//				radarObjects[i].layer = LayerMask.NameToLayer("Invisible");
//			}//end if
//			else{
//				borderObjects[i].layer = LayerMask.NameToLayer("Invisible");
//				radarObjects[i].layer = LayerMask.NameToLayer("RadarLayer");
//			}//end else
//		}//end for
	}//end Update

	void createRadarObjects(){
		radarObjects = new List<GameObject>();
		//borderObjects = new List<GameObject>();
		foreach (GameObject o in trackedObjects) {
			GameObject x = Instantiate (enemyPrefab, o.transform.position, Quaternion.identity) as GameObject;
			x.transform.parent = o.transform;
			radarObjects.Add (x);
//			GameObject y = Instantiate (enemyPrefab, o.transform.position, Quaternion.identity) as GameObject;
//			radarObjects.Add (y);
		}//end foreach
	}//end createRadarObjects
}//end class
