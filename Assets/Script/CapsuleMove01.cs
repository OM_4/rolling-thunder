﻿using UnityEngine;
using System.Collections;

public class CapsuleMove01 : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetKey ("q"))
		{
			Vector3 temp = new Vector3(0, 0.5f, 0);
			transform.position += temp;
		}
		if (Input.GetKey ("e"))
		{
			Vector3 temp = new Vector3(0, -0.5f, 0);
			transform.position += temp;
		}
		if (Input.GetKey ("w"))
		{
			Vector3 temp = new Vector3(0, 0, 0.5f);
			transform.position += temp;
		}
		if (Input.GetKey ("s"))
		{
			Vector3 temp = new Vector3(0, 0, -0.5f);
			transform.position += temp;
		}
		if (Input.GetKey ("d"))
		{
			Vector3 temp = new Vector3(0.5f, 0, 0);
			transform.position += temp;
		}
		if (Input.GetKey ("a"))
		{
			Vector3 temp = new Vector3(-0.5f, 0, 0);
			transform.position += temp;
		}
	}
}
