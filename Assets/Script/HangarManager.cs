﻿using UnityEngine;
using System.Collections;

public class HangarManager : MonoBehaviour {
	GameObject[] turretButtons;
	GameObject[] engineButtons;
	GameObject[] armorButtons;

	Vector3 newCamPos;
	
	GameObject obj;
	bool atNewPos = true;

	public Camera mainCam;

	public enum buttonIDs {Turret=1, Engine, Armor};

	void Start(){
		turretButtons = GameObject.FindGameObjectsWithTag ("Turrets");
		engineButtons = GameObject.FindGameObjectsWithTag ("Engines");
		armorButtons = GameObject.FindGameObjectsWithTag ("Armor");
	}

	void Update(){
		if (!atNewPos) {
			if(mainCam.transform.position.z <= newCamPos.z){
				mainCam.transform.Translate (Vector3.forward * 400 * Time.deltaTime);
				if(mainCam.transform.position.x <= newCamPos.x - 1 || mainCam.transform.position.x >= newCamPos.x + 1){
					mainCam.transform.Translate (Vector3.right * 50 * Time.deltaTime);
				}
			}else{
				atNewPos = true;
			}
		}
	}

	//Handles button input
	public void ButtonInput(int buttonID){
		//1 is turret button, 2 is armor, 3 is engine
		if ( buttonID == 1) {
			foreach(GameObject turret in turretButtons){
				turret.SetActive(true);
			}
			foreach(GameObject engine in engineButtons){
				engine.SetActive(false);
			}
			foreach(GameObject armor in armorButtons){
				armor.SetActive(false);
			}
		} else if ( buttonID == 2) {
			foreach(GameObject turret in turretButtons){
				turret.SetActive(false);
			}
			foreach(GameObject engine in engineButtons){
				engine.SetActive(false);
			}
			foreach(GameObject armor in armorButtons){
				armor.SetActive(true);
			}
		} else if ( buttonID == 3) {
			foreach(GameObject turret in turretButtons){
				turret.SetActive(false);
			}
			foreach(GameObject engine in engineButtons){
				engine.SetActive(true);
			}
			foreach(GameObject armor in armorButtons){
				armor.SetActive(false);
			}
		}
	}

	public void PartInput(string buttonName){
		obj = GameObject.Find (buttonName);

		newCamPos = new Vector3(obj.transform.position.x, obj.transform.position.y, obj.transform.position.z - 230);

		atNewPos = false;
	}
}
