using UnityEngine;
using System.Collections;

public class Health_Crew : MonoBehaviour 
{
	// These variables are used for the player's health and health bar
	public float curHP=100;
	public float maxHP=100;
	public float maxBAR=100;
	public float HealthBarLength;
	
	
	void OnGUI()
	{
		// This code creates the health bar at the coordinates 10,10
		GUI.Box(new Rect(10,10,HealthBarLength,25), "");
		// This code determines the length of the health bar
		HealthBarLength=curHP*maxBAR/maxHP;
	}
	
	void ChangeHP(float Change)
	{
		// This line will take whatever value is passed to this function and add it to curHP.
		curHP+=Change;
		
		// This if statement ensures that we don't go over the max health
		if(curHP>maxHP)
		{
			curHP=100;
		}
		
		// This if statement is to check if the player has died
		if(curHP<=0)
		{
			// Die
			Debug.Log("Player has died!");
		}
	}
	
	// This function checks if the player has entered a trigger
	void OnTriggerEnter(Collider other)
	{
		// The switch statement checks what tag the other gameobject is, and reacts accordingly.
		switch(other.gameObject.tag)
		{
		case "30Cal":
			ChangeHP(-1);
			break;
		case "50Cal":
			ChangeHP(-10);
			break;
		case "20MM":
			ChangeHP(-20);
			break;
		case "30MM":
			ChangeHP(-50);
			break;
		}
	}
}
