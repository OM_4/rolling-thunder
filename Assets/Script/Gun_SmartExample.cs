﻿using UnityEngine;
using System.Collections;

// EXAMPLE: TRIES TO FIRE WHEN AIMING "CLOSE ENOUGH".
public class Gun_SmartExample : Gun {

	public Transform target;
	public float ToleranceAngleDegrees = 3f;

	protected override void Update()
	{
		if (timer < period) // Can't fire yet
		{
			timer += Time.deltaTime;
		}
		// Is the gun pointed at the target yet?
		float angle = Vector3.Angle(transform.forward, target.position - transform.position);
		if (angle > ToleranceAngleDegrees)
		{
			AimAt(target.position);
		}
		else if (timer >= period) // Pointed at the target and ready to fire!
		{
			Fire ();
		}
	}
}
