﻿using UnityEngine;
using System.Collections;
using System;

public class TurretAIBarrelTest03 : MonoBehaviour {
	public GameObject item;
	public GameObject self;
	public Vector3 resultantVector;
	public double desiredAngle;
	public double desiredAngleRads;
	public double desiredDirection;
	public double newAngley;
	public float speed;
	public bool inRange;
	public double closeness;
	public double accuracy;
	public bool displayDesiredAngle;
	// Use this for initialization
	void Start () {
		speed = 2;
	}
	
	// Update is called once per frame
	void Update () {
		/*testNum ++;
		Transform bob = GetComponent<Transform>();
		Vector3 temp = new Vector3(testNum,0,0);
		bob.transform.rotation = temp;
		*/
		//item = GameObject.Find ("Capsule");
		resultantVector = (item.transform.position - self.transform.position);
		desiredDirection = Math.Sqrt (resultantVector.x * resultantVector.x + resultantVector.z * resultantVector.z);
		desiredAngleRads = Math.Atan (resultantVector.y / desiredDirection);
		desiredAngle = ( desiredAngleRads * (180 / Math.PI) );
		//Debug.Log (desiredAngleRads);

		if (desiredAngle < 0)
		{
			desiredAngle = 360 - Math.Abs(desiredAngle);
		}
		else if (desiredAngle > 360)
		{
			desiredAngle = desiredAngle - 360;
		}
		if (displayDesiredAngle){
			Debug.Log (desiredAngle);
		}
		/*
		if (resultantVector.z < 0)
		{
			desiredAngle = 180 + desiredAngle;
		}
		else if (resultantVector.x < 0)
		{
			desiredAngle = 360 + desiredAngle;
		}
		*/
		newAngley = self.transform.rotation.eulerAngles.x;
		
		if (newAngley < 0)
		{
			newAngley = 360 - Math.Abs(newAngley);
		}
		else if (newAngley > 360)
		{
			newAngley = newAngley - 360;
		}

		//desiredAngle = 360 - desiredAngle; 

		Vector3 newVel = new Vector3 (1 * speed, 0, 0);
		Vector3 newVel2 = new Vector3 (-1 * speed, 0, 0);

		closeness =  desiredAngle - newAngley;

		if (Math.Abs(closeness) < accuracy)
		{
			inRange = true;
		}
		else if (desiredAngle > newAngley)
		{
			inRange = false;
			self.transform.Rotate(newVel);
		}
		else if (desiredAngle < newAngley)
		{
			inRange = false;
			self.transform.Rotate(newVel2);
		}
		
	}
}
