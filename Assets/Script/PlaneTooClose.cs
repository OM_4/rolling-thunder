﻿using UnityEngine;
using System.Collections;

public class PlaneTooClose : MonoBehaviour {
	public GameObject plane;
	[HideInInspector] public AI_Harasser harasserScript;

	void Awake(){
		harasserScript = plane.GetComponent<AI_Harasser>();
	}
	
	void OnTriggerEnter(Collider collideData) {
		if(collideData.CompareTag("Allied") || collideData.CompareTag ("AlliedPlayer")){
			harasserScript.setTooClose();
		}

	}	
}
