﻿using UnityEngine;
using System.Collections;

public class FlakTrigger : MonoBehaviour {

		
	public static bool triggerFlak;


		// Use this for initialization
		void Start () {
			
			triggerFlak = false;
			
		}
		
		// Update is called once per frame
		void OnTriggerStay (Collider other){
			
			if(other.tag == "Allied") 
			{
				triggerFlak = true;

		}
		
	}
	
	void OnTriggerExit (Collider other){
		
		if(other.tag == "Allied") 
		{
			triggerFlak = false;
			
		}
		
	}
}
