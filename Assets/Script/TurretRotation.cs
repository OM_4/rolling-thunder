﻿using UnityEngine;
using System.Collections;

public class TurretRotation : MonoBehaviour {

	public Transform Target;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
		if(InRangeTest.turretActive == true)
		{
			Target = GameObject.FindGameObjectWithTag ("Axis").transform;

			transform.LookAt(Target);
		}

	}
}
