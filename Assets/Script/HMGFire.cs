﻿using UnityEngine;
using System.Collections;

public class HMGFire : MonoBehaviour {
	
	public bool inRange;
	
	// Use this for initialization
	void Start () {
		
		inRange = false;
		
	}
	
	// Update is called once per frame
	void OnTriggerStay (Collider other){
		
		if(other.tag == "Axis") 
		{
			inRange = true;
			
		}
		
	}
	
	void OnTriggerExit (Collider other){
		
		if(other.tag == "Axis") 
		{
			inRange = false;
			
		}
		
	}
}
