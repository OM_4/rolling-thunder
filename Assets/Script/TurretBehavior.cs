﻿using UnityEngine;
using System.Collections;

public class TurretBehavior : MonoBehaviour {

	public GameObject guns;

	// Use this for initialization
	void Start () {

		guns = GameObject.Find ("a_turret1");
	}
	
	// Update is called once per frame
	void Update () {
		guns.transform.Rotate(0, 100 * Time.deltaTime, 0);
	}
}
