﻿using UnityEngine;
using System.Collections;

// Base class for plane AI. Do NOT attach!
public class AI_Base : MonoBehaviour
{
	[HideInInspector] public new Transform transform;
	[HideInInspector] public new Rigidbody rigidbody;
	public Transform target;
	protected Rigidbody targetRigidbody;
	
	protected FlightMovement movement;
	
	public float NormalSpeed = 40f;

	protected virtual void Awake()
	{
		transform = GetComponent<Transform>();
		rigidbody = GetComponent<Rigidbody>();
		movement = GetComponent<FlightMovement>();
		if (target != null)
		{
			SetTarget (target);
			transform.LookAt (target);
		}
	}
	
	public void SetTarget(Transform t)
	{
		target = t;
		targetRigidbody = t.GetComponent<Rigidbody>();
	}

}
