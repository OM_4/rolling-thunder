﻿using UnityEngine;
using System.Collections;

public class AI_Kamikaze : AI_Base {

	public float MaxTurnMultiplier = 2f;
	
	void Update()
	{
		Vector3 dist = (target.position+targetRigidbody.velocity*Time.deltaTime) - transform.position;
		Vector3 direction = dist.normalized;
		float hdiff = Vector3.Dot (direction, transform.right);
		float vdiff = -Vector3.Dot (direction, transform.up);
		float angle = Vector3.Angle (direction, transform.forward);
		//angle = Mathf.Max(angle, MaxAngle);
		hdiff = Mathf.Max(-MaxTurnMultiplier, Mathf.Min(hdiff*angle, MaxTurnMultiplier));
		vdiff = Mathf.Max(-MaxTurnMultiplier, Mathf.Min(vdiff*angle, MaxTurnMultiplier));
		
		movement.axisHorizontal = hdiff;
		movement.axisVertical = vdiff;
		
		// Speed up or slow down depending on how sharp the turn is
		// Values are arbitrary but seemed to test well
		if (angle >= 90f) movement.targetSpeed = NormalSpeed*0.1f;
		else if (angle >= 60f) movement.targetSpeed = NormalSpeed*0.6f;
		else if (angle >= 45f) movement.targetSpeed = NormalSpeed;
		else if (angle >= 30f) movement.targetSpeed = NormalSpeed*1.25f;
		else if (angle >= 10f) movement.targetSpeed = NormalSpeed*1.75f;
		else movement.targetSpeed = NormalSpeed*2f;
	}
}
