﻿using UnityEngine;
using System.Collections;

public class AI_Bruiser : AI_Base {
	
	public float MaxTurnMultiplier = 2f;
	[HideInInspector] public int behavior = 0;
	[HideInInspector] public float startTime;
	[HideInInspector] public float timeSinceRun;
	[HideInInspector] public float speed = 0.8f;
	[HideInInspector] public float step;
	//private Vector3 runVector = new Vector3 (0.0f, 0.0f, 0.0f);
	public Vector3 direction;
	public Vector3 dist;
	
	public float rotationSpeed;
	
	private Quaternion lookRotation;
	private Vector3 direction1;
	private float runValue;
	
	public FlightMovement flightScript;
	
	[HideInInspector] public GameObject closest;
	
	void Start(){
		flightScript = transform.GetComponent<FlightMovement> ();
	}
	
	
	void Update()
	{
		if (behavior == 0) {
			startTime = Time.timeSinceLevelLoad;
			chasing();
		} else if (behavior == 1) {
			findNextTarget();
			//runVector = new Vector3(0.0f, 0.0f, 0.0f);
		}else {
			hug();
		}
	}
	
	public void setTooClose(){
		//Debug.Log ("Set too close");
		behavior = 2; 

	}
	
	public void hug(){
		timeSinceRun = Time.timeSinceLevelLoad - startTime;
		
		if (timeSinceRun <= 5.0) {
			if(flightScript.targetSpeed > 10.0f){
				flightScript.targetSpeed -= 0.5f;
			}
		} else {
			behavior = 1;
		}
	}
	
	public void chasing(){
		//dist = (target.position + runVector + targetRigidbody.velocity * Time.deltaTime) - transform.position;
		dist = (target.position + targetRigidbody.velocity * Time.deltaTime) - transform.position;
		direction = dist.normalized;
		float hdiff = Vector3.Dot (direction, transform.right);
		float vdiff = -Vector3.Dot (direction, transform.up);
		float angle = Vector3.Angle (direction, transform.forward);
		//angle = Mathf.Max(angle, MaxAngle);
		hdiff = Mathf.Max(-MaxTurnMultiplier, Mathf.Min(hdiff*angle, MaxTurnMultiplier));
		vdiff = Mathf.Max(-MaxTurnMultiplier, Mathf.Min(vdiff*angle, MaxTurnMultiplier));
		
		movement.axisHorizontal = hdiff;
		movement.axisVertical = vdiff;
		//Debug.Log ("HDiff and VDiff");
		//Debug.Log (hdiff);
		//Debug.Log (vdiff);
		
		if (dist.magnitude > 75.0f) {
			transform.LookAt (target);
		}
		
		// Speed up or slow down depending on how sharp the turn is
		// Values are arbitrary but seemed to test well
		if (angle >= 90f) movement.targetSpeed = NormalSpeed*0.1f;
		else if (angle >= 60f) movement.targetSpeed = NormalSpeed*0.6f;
		else if (angle >= 45f) movement.targetSpeed = NormalSpeed;
		else if (angle >= 30f) movement.targetSpeed = NormalSpeed*1.25f;
		else if (angle >= 10f) movement.targetSpeed = NormalSpeed*1.75f;
		else movement.targetSpeed = NormalSpeed*2f;
	}
	
	public void findNextTarget(){
		SetTarget(FindClosestEnemy ());
		//Debug.Log ("Target name: " + target.name);
		behavior = 0;
	}
	
	Transform FindClosestEnemy() {
		GameObject[] gos;
		GameObject[] gos2;
		GameObject[] combined;
		gos = GameObject.FindGameObjectsWithTag("AlliedPlayerPlane");
		gos2 = GameObject.FindGameObjectsWithTag ("AlliedPlane");
		
		combined = new GameObject[gos.Length + gos2.Length];
		
		gos.CopyTo (combined, 0);
		gos2.CopyTo (combined, gos.Length);
		
		float distance = Mathf.Infinity;
		Vector3 position = transform.position;
		foreach (GameObject go in combined) {
			Vector3 diff = go.transform.position - position;
			float curDistance = diff.sqrMagnitude;
			if (curDistance < distance) {
				closest = go;
				distance = curDistance;
			}
		}
		return closest.transform;
	}
}