
using UnityEngine;
using System.Collections;

public class ClickToFireRevised : MonoBehaviour {
	
	//public GameObject bullet;
	private bool keepFiring = false;
	//private float startTime;
	public float FireRate = 0.7f;
	
	public GameObject pooler;
	public ObjectPoolerScript objPoolScript;
	public Vector3 modRotate;
	
	public GameObject barrel;
	public float offset = 0.0f;
	
	// Use this for initialization
	void Start() {
		//this.enabled = false;
		this.enabled = true;
	}
	
	void Update(){
		if (Input.GetMouseButton(0)) {
			keepFiring = true;
			}
		else {

			keepFiring = false;
		}
		
	}

	
	void OnEnable(){
		InvokeRepeating ("FireRateFunc", FireRate, FireRate);
	}
	
	void FireRateFunc(){
		if (!gameObject.activeInHierarchy) {
			keepFiring = false;
		}
		
		if (keepFiring) {
			Fire ();
		}
	}
	
	void Fire(){
		//Instantiate (bullet, target.position, transform.rotation);
		objPoolScript = (ObjectPoolerScript)pooler.GetComponent<ObjectPoolerScript> ();
		GameObject obj = objPoolScript.GetPooledObject ();
		
		if (obj == null) return;
		
		modRotate = barrel.transform.position;
		modRotate.z += offset;
		modRotate.y += 0;
		obj.transform.position = modRotate;
		obj.transform.rotation = barrel.transform.rotation;
		obj.SetActive (true);
		audio.Play ();
	}
}
