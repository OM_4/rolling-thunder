﻿using UnityEngine;
using System.Collections;

public class SpawnScript : MonoBehaviour {
	public Transform target;
	public Transform prefab;

	public GameObject pooler;
	public ObjectPoolerScript objPoolScript;

	public GameObject bulletPooler;
	public BasicGun basicGunScript;
	public HealthBar healthScript;

	public Vector3 position;
	[HideInInspector] public int numHarassers = 0;
	[HideInInspector] public int numBruisers = 0;
	[HideInInspector] public int numKamikaze = 0;
	public AI_Harasser harasserScript;

	public Transform prefabReturn;

	public float SpawnRate;
	private float startTime;

	// Use this for initialization
	void Start () {
		startTime = Time.time;
		position = target.position;
		position.y += 700;
		this.gameObject.transform.position = position;
	}
	
	// Update is called once per frame
	void Update () {
		position = target.position;
		position.y += 700;
		this.gameObject.transform.position = position;

		if (numHarassers < 10) {
			if(Time.time - startTime >= SpawnRate){
				Spawn();
				startTime = Time.time;
			}
		}
	}

	void Spawn(){
		objPoolScript = (ObjectPoolerScript)pooler.GetComponent<ObjectPoolerScript> ();
		GameObject obj = objPoolScript.GetPooledObject ();

		if (obj == null) {
			return;
		}

		obj.transform.position = transform.position;
		obj.transform.rotation = transform.rotation;
		obj.SetActive (true);

		//prefabReturn = (Transform) Instantiate(prefab, this.gameObject.transform.position, this.gameObject.transform.rotation);
		AI_Harasser harasserScript = (AI_Harasser)obj.GetComponent<AI_Harasser>();
		HealthBar healthScript = (HealthBar)obj.GetComponentInChildren<HealthBar> ();
		harasserScript.SetTarget(target);
		healthScript.ResetHealth ();

		foreach (Transform child in obj.transform) {
			if(child.gameObject.CompareTag("Blast")){
				BasicGun basicGun = (BasicGun)child.GetComponent<BasicGun>();
				basicGun.SetPooler(bulletPooler);
			}
		}
		numHarassers += 1;
	}
}
