﻿using UnityEngine;
using System.Collections;

public class ThrottleControl : MonoBehaviour {

	public Transform plane;
	FlightMovement flightScript;

	// Use this for initialization
	void Start () {
		flightScript = plane.GetComponent<FlightMovement> ();
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetKeyDown (KeyCode.E)) {
			if(flightScript.targetSpeed < 35){
			 	flightScript.targetSpeed += 5.0f;
			}
		}else if(Input.GetKeyDown (KeyCode.R)){
			if(flightScript.targetSpeed > 15){
				flightScript.targetSpeed -= 5.0f;
			}
		}
	}
}
