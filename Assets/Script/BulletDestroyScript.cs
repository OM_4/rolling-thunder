﻿using UnityEngine;
using System.Collections;

public class BulletDestroyScript : MonoBehaviour {
	public float SecondsUntilDestroy = 0.5f;

	void OnEnable(){
		Invoke ("Destroy", SecondsUntilDestroy);
	}

	void Destroy(){
		gameObject.SetActive (false);
	}

	void OnDisable(){
		CancelInvoke ();
	}
}
