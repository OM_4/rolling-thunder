﻿using UnityEngine;
using System.Collections;

public class BasicGunAllied : MonoBehaviour {
	
	//public GameObject bullet;
	public GameObject pooler;
	public ObjectPoolerScript objPoolScript;

	private bool keepFiring = false;
	//private float startTime;
	public float FireRate = 0.1f;
	public Transform target;
	
	// Use this for initialization
	void Start() {
		InvokeRepeating ("FireRateFunc", FireRate, FireRate);
	}
	
	// Update is called once per frame
	//void LateUpdate () {
	//	if (keepFiring) {
	//		if(Time.time - startTime >= FireRate){
	//			Fire ();
	//			startTime = Time.time;
	//		}
	//	}
	//}

	void FireRateFunc(){
		if (keepFiring) {
			Fire ();
		}
	}
	
	void Fire(){
		//Instantiate (bullet, target.position, transform.rotation);
		//AI_Harasser harasserScript = (AI_Harasser)prefabReturn.GetComponent<AI_Harasser>();
		objPoolScript = (ObjectPoolerScript)pooler.GetComponent<ObjectPoolerScript> ();
		GameObject obj = objPoolScript.GetPooledObject ();

		if (obj == null) return;

		obj.transform.position = target.position;
		obj.transform.rotation = transform.rotation;
		obj.SetActive (true);
	}
	
	void OnTriggerEnter(Collider collideData) {
		//Debug.Log ("Object triggered collision");
		//Debug.Log (collideData.tag);
		if (!keepFiring) {
			if (collideData.transform.CompareTag("Enemy")) {
				//Debug.Log ("Target found");
				//Debug.Log (collideData.transform.tag);
				keepFiring = true;
			}
		}
	}
	
	void OnTriggerExit(Collider collideData) {
		if (collideData.transform.CompareTag("Enemy")) {
			//Debug.Log ("Object exited area");
			//Debug.Log (collideData.transform.tag);
			keepFiring = false;
		}
	}
	
}