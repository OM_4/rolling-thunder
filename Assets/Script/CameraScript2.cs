﻿using UnityEngine;
using System.Collections;

public class CameraScript2 : MonoBehaviour {
	Vector3 rotateVect = new Vector3(0, 0, 0);
	float minY = -20f;
	float maxY = 20f;
	float minX = -10f;
	float maxX = 5;
	float rotateSpeed = 45.0f;
	
	
	// Use this for initialization
	void Start () {
		transform.localEulerAngles = rotateVect;
	}
	
	// Update is called once per frame
	void LateUpdate () {
		rotateVect.x += Input.GetAxis ("Mouse Y") * rotateSpeed * Time.deltaTime;
		rotateVect.x = Mathf.Clamp (-rotateVect.x, minY, maxY);
		
		transform.localEulerAngles = rotateVect;
	}
}
