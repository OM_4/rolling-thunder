﻿using UnityEngine;
using System.Collections;

public class BulletImpactScript : MonoBehaviour {

	public void OnCollisionEnter(Collision node){
		if (node.gameObject.tag == "Enemy")
						Debug.Log ("destroyed");
		{
			Destroy(node.gameObject);
		}
	}
}
