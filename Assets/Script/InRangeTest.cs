﻿using UnityEngine;
using System.Collections;

public class InRangeTest : MonoBehaviour {

	public static bool turretActive;

	// Use this for initialization
	void Start () {

		turretActive = false;
	
	}
	
	// Update is called once per frame
	void OnTriggerStay (Collider other){

		if(other.tag == "Axis") 
		{
			turretActive = true;

		}
	
}

	void OnTriggerExit (Collider other){
		
		if(other.tag == "Axis") 
		{
			turretActive = false;
			
		}
		
	}
}
