﻿using UnityEngine;
using System.Collections;

public class SwitchGun : MonoBehaviour {
	
	
	public Camera Dorsal1;
	public Camera Dorsal2;
	public Camera Ventral_Ball;
	

	private Camera[] TurretCameras;
	private int currentTurretCameraIndex = 0;
	private Camera currentTurretCamera;
	
	
	void Start()
	{
		
		{
			TurretCameras = new Camera[] { Dorsal1, Dorsal2, Ventral_Ball };//this is the array of cameras
			currentTurretCamera = Dorsal1; //When the program start the main camera is selected as the default camera
			ChangeTurretView ();
		}
	}

	void TurretUpdate()
	{
		if(Input.GetKeyDown("t"))
		{
			currentTurretCameraIndex++;
			if (currentTurretCameraIndex > TurretCameras.Length-1)
				currentTurretCameraIndex = 0;
			ChangeTurretView();
		}
	}
	
	void ChangeTurretView()
	{
		currentTurretCamera.enabled = false;
		currentTurretCamera = TurretCameras[currentTurretCameraIndex];
		currentTurretCamera.enabled = true;
	}
}