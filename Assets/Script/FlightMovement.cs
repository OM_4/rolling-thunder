﻿using UnityEngine;
using System.Collections;

// GENERIC FLIGHT MOVEMENT SYSTEM FOR PLANES
[RequireComponent(typeof(Rigidbody))]
public class FlightMovement : MonoBehaviour {

	//Variables for speed
	public float speed = 20f;
	public float targetSpeed = 40.0f;
	public float maxSpeed = 250f; // mph
//	public float maxDiveSpeed = 300f; // mph
//	public float minSpeed = 1f;
	//Variables for throttle
	public float throttle = 1.0f;
	//public float maxThrottle = 20.0f;
	//Variable for roll speed and pitch speed
	public float rollSpeed = 15f;
	public float yawSpeed = 20f;
	public float pitchSpeed = 15f;
	public float maxCorrectionRate=0.5f;
	
	
	public bool stall = false;
	
	// Set these values to change direction and angle
	public float axisVertical = 0f;
	public float axisHorizontal = 0f;

	[HideInInspector] public new Transform transform;
	[HideInInspector] public new Rigidbody rigidbody;
	
	//float mass;
	
	//Quaternion upOrientation;
	

	void Awake() {
		transform = GetComponent<Transform> (); //cached
		rigidbody = GetComponent<Rigidbody> (); //cached
		rigidbody.velocity = transform.forward * speed;
	}

	// This isn't really how flight works, but it's a decent simulation
	void FixedUpdate()
	{
		//speed = (transform.rotation * rigidbody.velocity).z; // FORWARD speed
		float dt = Time.deltaTime;
		if (stall)
		{
			if(speed < maxSpeed){
				speed += 9.8f * dt;
			}
			
			if(transform.rotation.eulerAngles.x < 80.0f || transform.rotation.eulerAngles.x > 90.0f){
				if(transform.rotation.eulerAngles.z > 6.0f && transform.rotation.eulerAngles.z < 180.0f){
					transform.Rotate(0.8f, 0.0f, -0.8f);
				}else if(transform.rotation.eulerAngles.z >= 180.0f && transform.rotation.eulerAngles.z < 350.0f){
					transform.Rotate(0.8f, 0.0f, 0.8f);
				}else{
					transform.Rotate(0.8f, 0.0f, 0.0f);
				}
			}
			else{
				stall = false;
			}
		}
		else // Normal flight, not stalling
		{
			
			// This will produce the speed-flicker effect
			if (speed >= targetSpeed)
			{
				speed -= dt;
			}
//			speed += dt*throttle;
			
			speed = Mathf.Lerp(speed, targetSpeed, dt*throttle);// * throttle);
//			speed -= transform.forward.y * dt * 50f;

//			if (speed > targetSpeed) {
//				speed -= 5;
//			}
//			else if (speed <= minSpeed) {
//				stall= true;
//				rigidbody.AddForce(transform.forward);
//				//speed = minSpeed;
//			}

			if(transform.CompareTag("AlliedPlayer")){
				transform.Rotate (pitchSpeed * Input.GetAxis("Vertical") * dt, 0f, rollSpeed * -Input.GetAxis("Horizontal") * dt);
			}else{
				transform.Rotate (pitchSpeed * axisVertical * dt, 0f, rollSpeed * -axisHorizontal * dt);
			}

		}
		// We only use gravity for speeding up or slowing down from pitch
		speed -= transform.forward.y * 9.8f * dt;
		// Roll -> Yaw
		float turnRate = yawSpeed * Vector3.Dot(transform.right, Vector3.up);
		transform.RotateAround(transform.position, Vector3.down, turnRate*dt);
		// If plane isn't oriented normally, gradually roll back
		if (turnRate > maxCorrectionRate) turnRate = maxCorrectionRate;
		else if (turnRate < -maxCorrectionRate) turnRate = -maxCorrectionRate;
		transform.Rotate(0, 0, -5f * dt * turnRate);
		rigidbody.velocity = transform.forward * speed;// + Vector3.up*lift;
	}
}
