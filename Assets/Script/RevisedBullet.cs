﻿using UnityEngine;
using System.Collections;

public class RevisedBullet : MonoBehaviour {
	public float Speed = 0.9f;
	
	void Update() {
		this.gameObject.transform.position += Speed * this.gameObject.transform.up;
	}
}