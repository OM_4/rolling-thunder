﻿using UnityEngine;
using System.Collections;

public class TurretOrientation : MonoBehaviour {
	public Transform turretTarget;

	public void Update(){
		//Do nothing
	}

	public void track(Transform plane){
		turretTarget = plane;
		this.transform.LookAt(turretTarget);
	}
}
