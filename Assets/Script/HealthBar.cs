﻿using UnityEngine;
using System.Collections;

public class HealthBar : MonoBehaviour 
{
	// These variables are used for the player's health and health bar
	public float curHP=100;
	public float maxHP=100;
	public float maxBAR=100;
	public float maxBARA=100;
	public float curArmor=100;
	public float maxArmor=100;
	public bool armorGone = false;
	public float HealthBarLength;
	public float ArmorBarLength;
	public GameObject target;
	public GameObject smokeTrail;
	public bool destroyWhole = false;
	
	/*
    void Update (){
        if (curHP == 0) {
            Destroy (this.gameObject);
        }
    }
    */
	
	void ChangeArmor(float Change)
	{
		// This line will take whatever value is passed to this function and add it to curHP.
		curArmor += Change;
		
		// This if statement ensures that we don't go over the max health
		if(curArmor > maxArmor)
		{
			curArmor = 100;
		}
		
		// This if statement if armor is gone
		if(curArmor <= 0)
		{
			// Die
			//Debug.Log("Armor Gone!");
			armorGone = true;
		}
	}
	
	void ChangeHP(float Change)
	{
		// This line will take whatever value is passed to this function and add it to curHP.
		curHP += Change;
		
		// This if statement ensures that we don't go over the max health
		if(curHP > maxHP)
		{
			curHP = 100;
		}
		
		if (curHP <= 18) {
			smokeTrail.SetActive(true);
		}
		
		// This if statement is to check if the player has died
		if(curHP <= 0)
		{
			// Die
			//Debug.Log("Part of plane destroyed!");
			if(destroyWhole){
				//Debug.Log ("Enemy destroyed!!!");
				this.transform.parent.parent.gameObject.SetActive(false);
			}
			target.SetActive (false);
		}
	}
	
	public void ResetHealth(){
		curHP = maxHP;
		curArmor = maxArmor;
	}
	
	// This function checks if the player has entered a trigger
	void OnTriggerEnter(Collider other)
	{
		///Debug.Log ("Armor");
		//Debug.Log (curArmor);
		//Debug.Log ("Health");
		//Debug.Log (curHP);
		if (other.gameObject.CompareTag("Bullet")) {
			//Debug.Log ("Bullet entered area");
			if (!armorGone) {
				switch (other.gameObject.tag) {
				case "Bullet":
					ChangeArmor (-1);
					break;
				case "50Cal":
					ChangeArmor (-10);
					break;
				case "20MM":
					ChangeArmor (-25);
					break;
				}
			}
		}
	}
}
