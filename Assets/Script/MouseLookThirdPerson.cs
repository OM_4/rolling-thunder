﻿using UnityEngine;
using System.Collections;

[AddComponentMenu("Camera-Control/Mouse Look")]
public class MouseLookThirdPerson : MonoBehaviour {

	public GameObject target;
	public float rotateSpeed = 10;

	void LateUpdate() {
		transform.RotateAround( target.transform.position, target.transform.up, Input.GetAxis("Mouse X") * rotateSpeed );
	}
}