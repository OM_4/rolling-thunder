﻿using UnityEngine;
using System.Collections;

public class EnemyTestAI : MonoBehaviour
{

	public Transform Target;
	public float Speed = 30.0f;
	public float maxSpeed = 50.0f;
	

	void Start ()
	{
		Target = GameObject.FindGameObjectWithTag("Allied").transform;
	}
	

	void Update ()
	{
		transform.LookAt(Target);

		transform.position += transform.forward * Time.deltaTime * Speed;

		Speed -= transform.forward.y * Time.deltaTime * 1.0f;


}
}


	
