﻿using UnityEngine;
using System.Collections;

public class BasicGun : MonoBehaviour {

	//public GameObject bullet;
	private bool keepFiring = false;
	//private float startTime;
	public float FireRate = 0.1f;

	public GameObject pooler;
	public ObjectPoolerScript objPoolScript;
	
	// Use this for initialization
	void Start() {
		InvokeRepeating ("FireRateFunc", FireRate, FireRate);
	}
	
	void FireRateFunc(){
		if (!gameObject.activeInHierarchy) {
			keepFiring = false;
		}

		if (keepFiring) {
			Fire ();
		}
	}
	
	void Fire(){
		//Instantiate (bullet, target.position, transform.rotation);
		objPoolScript = (ObjectPoolerScript)pooler.GetComponent<ObjectPoolerScript> ();
		GameObject obj = objPoolScript.GetPooledObject ();
		
		if (obj == null) return;
		
		obj.transform.position = transform.position;
		obj.transform.rotation = transform.rotation;
		obj.SetActive (true);
	}
	
	void OnTriggerEnter(Collider collideData) {
		//Debug.Log ("Object triggered collision");
		//Debug.Log (collideData.tag);
		if (!keepFiring) {
			if (collideData.transform.CompareTag("Enemy")) {
				//Debug.Log ("Target found");
				//Debug.Log (collideData.transform.tag);
				keepFiring = true;
			}
		}
	}
	
	void OnTriggerExit(Collider collideData) {
		if (collideData.transform.CompareTag("Enemy")) {
			//Debug.Log ("Object exited area");
			//Debug.Log (collideData.transform.tag);
			keepFiring = false;
		}
	}

	public void SetPooler(GameObject g){
		pooler = g;
	}
}
