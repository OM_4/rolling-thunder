﻿using UnityEngine;
using System.Collections;

public class WinGame : MonoBehaviour {
	public GameObject plane;
	public Camera winCamera;
	public Camera loseCamera;
	public bool keepWin = false;

	void Update(){
		if (keepWin) {
			loseCamera.enabled = false;
		}
	}
	
	void OnTriggerEnter(Collider collideData) {
		//Debug.Log ("Object triggered collision");
		//Debug.Log (collideData.tag);
			//if (collideData.transform.CompareTag("Player")) {
				plane.SetActive (false);
				loseCamera.enabled = false;
				winCamera.enabled = true;
				keepWin = true;
			//}
	}
}
