﻿using UnityEngine; 
using System.Collections;

public class CameraZoom : MonoBehaviour {
	
	public float zoomSpeed = 20;
	
	void Update () {
		
		float scroll = Input.GetAxis("Mouse ScrollWheel");
		if (scroll != 0.0f)
		{
			camera.fieldOfView += scroll*zoomSpeed;

		}
	} }